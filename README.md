# Selenoid Framework Overview #

## Introduction ##
Selenoid Framework is a generic open source test automation framework for acceptance testing. It uses Selenium and is written in Java. The core module (selenoid) delivers all functionalities for writing and executing tests. You are able to create new projects, independent from the core module, so that the whole logic is separated. The framework uses Maven for test execution.

The Selenoid Framework is operating system and application independent. The core framework is implemented using Java. The framework has a rich ecosystem around it consisting of various generic test libraries and tools that are developed as separate projects.

## Main Features: ##
- JIRA Integration: failed tests can be pushed as ticket to JIRA
- TestRail Integration: push test results to TestRail
- Link Checker: grabs all links from a specific page (or from all pages of the domain) and validates that links are valid (checks the returned status code)
- Test Runner: Provides all needed selenium webdriver commands, wait methods and assertions.
- Driver: use own Remote Machines or SauceLabs as test environment

# Installation instructions #
*The following instructions are written for the usage of Eclipse.*

## Java 1.8 ##
- Install Java SE Development Kit 8 (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

## Eclipse ##
- Use "Eclipse IDE for Java EE Developers"
- http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/marsr or a newer version
- After Eclipse is installed, please make sure, that you use the correct Java Version (1.8)
-- Go to Eclipse > Settings
-- Go to "Java" > "Compiler" 
-- Compiler should use "1.8"

## Selenoid ##
1. Download selenoid core: clone repository to your local environment
2. Open eclipse and import project as existing maven project
-- Eclipe: File > Import > Maven > Existing Maven Projects
-- Select selenoid folder on your local environment
-- Select pom.xml and click on "Finish"
3. Maven install
-- Go to the project explorer in Eclipse 
-- Right click on the project "selenoid"
-- Go to "Run as" and select "Maven install"
4. Now, selenoid is ready to use!

*Please note: Selenoid is the core module for the tests. Tests are implementented in separate tests projects.*

## Example project ##

### Download & Import ###
1. Download selenoid-example: clone repository to your local environment
2. Open eclipse and import project as existing maven project
-- Eclipe: File > Import > Maven > Existing Maven Projects
-- Select selenoid folder on your local environment
-- Select pom.xml and click on "Finish"

### How to run a test ###

**Run tests on your local machine**
*Precondition: Chrome is installed on your environment*

1. In Eclipse, go to "Run > Run Configurations"
2. In the left tree, add a new build under "Maven Build"
3. Select the base directory (Browse Workspace.. > select "selenoid-example")
4. Insert the following parameters into Goals: "test -e -Dbrowser=chrome -Dxml=testsuite -Dlocal=true -Dtestrail=false -Djira=false -P live"
5. Apply and Run!

### How to write a test ###

**General test structure**
```
#!java

    @Test(description = "Short description of the test", groups = { Groups.DEV })
    public void exampleTest() {
        i.openHomepage(maven.getProperty("website"));
        i.see(Homepage.logo);
        i.see(Homepage.search);
        i.enterText(Homepage.search, "testautomation");
    }
```

**Pageobjects**
```
#!java
public class Homepage {
    public static By logo = By.id("hplogo");
    public static By search = By.id("lst-ib");
    public static By searchBtn = By.name("btnK");
}
```
**Configure testsuite**
The testsuite contains all test classes which should be executed. You can also filter the tests by groups which you can use for your tests. You can add multiple testsuites for different usages, for example a full.xml (run all tests), stage.xml (tests, which should be only executed on stage) or a live.xml (tests, which should be only executed on live). 

```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd" >

<suite name="Example Test Suite" parallel="false" verbose="1">
	<test name="Example tests">
    	<method-selectors> 
        	<method-selector> 
                 <script language="beanshell">
                 	<![CDATA[groups.containsKey("develop")]]>
               	</script>
            </method-selector> 
        </method-selectors> 
        <classes>
        	<class name="tests.example.ExampleTest" />
        </classes>
    </test>
</suite>

```

**pom.xml**
A Project Object Model or POM is the fundamental unit of work in Maven. It is an XML file that contains information about the project and configuration details used by Maven to build the project. More information: https://maven.apache.org/guides/introduction/introduction-to-the-pom.html

An important part for the Selenoid framework is the "Profiles" section, where you add the urls for executing your project (stage and live environment).
```
#!xml
        ...
	<!-- Profiles for environments -->
	<profiles>
   		<profile>
       		<id>staging</id>
      		<properties>
           		<environment>staging</environment>
           		<website>http://www.google.de</website>
       		</properties>
   		</profile>
   		<profile>
       		<id>live</id>
       		<properties>
           		<environment>live</environment>
    		    <website>http://www.google.de</website>
       		</properties>
   		</profile>
	</profiles>
  	...
```

## Setup a new project ##

*pom.xml*

# Insights (deep dive) #

## Projects ##

*src/main/java*
- pageobjects 
- settings
-- ProjectSettings.java
-- TestSuiteRemote.java
-- TestSuiteSauceLabs.java
*src/main/resources*
- application.properties
- chromedriver
- log4j2.xml
*src/test/resources*
- testsuite.xml
*src/test/java*
- tests.example

## Selenoid Core ##