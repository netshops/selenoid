package drivers.types;

/**
 * Available mobile devices provided by sauce labs
 * 
 * @author Philip Taddey
 */
public enum Mobile {
	IPHONE_SIMULATOR("iphone", "iPhone Simulator"),  
	IPHONE6PLUS("iphone6p", "iPhone 6 Plus"),  
	IPHONE6("iphone6", "iPhone 6"),  
	IPHONE5S("iphone5s", "iPhone 5s"),  
	IPHONE5("iphone5", "iPhone 5"),  
	IPHONE4S("iphone4s", "iPhone 4s"),  	
	SAMSUNG_GALAXY_S2("s2", "Samsung Galaxy S2 Emulator"), 
    SAMSUNG_GALAXY_S3("s3", "Samsung Galaxy S3 Emulator"), 
    SAMSUNG_GALAXY_S4("s4", "Samsung Galaxy S4 Emulator");

    private String shortName;
    private String saucelabsName;

    private Mobile(String shortName, String saucelabsName) {
        this.shortName = shortName;
        this.saucelabsName = saucelabsName;
    }

    /**
     * Provide the short name of the mobile interface
     * 
     * @return {@link String}
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Provide the saucelabs name of the mobile interface
     * 
     * @return {@link String}
     */
    public String getSaucelabsName() {
        return saucelabsName;
    }

    /**
     * Check if a given String is an expression of the specific type
     * 
     * @param s
     *            - the String for lookup
     * @return boolean
     */
    public boolean isPreciseInstanceOf(String s) {
        if (s.equalsIgnoreCase(this.getSaucelabsName())) return true;
        if (s.equalsIgnoreCase(this.getShortName())) return true;
        if (s.equalsIgnoreCase(this.name())) return true;
        return false;
    }
    
    /**
     * Check if a given String is available as type {@link Mobile}
     * 
     * @param s - given String to search for
     * @return {@link Mobile}
     */
    public static Mobile lookup(String s) {
        for(Mobile d : values())
            if(d.name().equalsIgnoreCase(s) || d.getShortName().equalsIgnoreCase(s) || d.getSaucelabsName().equalsIgnoreCase(s))
                return d;
        return null;
    }
}