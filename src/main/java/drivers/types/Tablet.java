package drivers.types;

/**
 * Available tablet devices provided by sauce labs
 * 
 * @author Philip Taddey
 */
public enum Tablet {
    SAMSUNG_TAB_3("tab3", "Samsung Galaxy Tab 3 Emulator"), 
    SAMSUNG_NOTE_10("note10", "Samsung Note 10.1 Emulator"), 
    GOOGLE_NEXUS_7_HD("nexus7", "Google Nexus 7 HD Emulator"), 
    IPAD2("ipad2", "iPad 2"),
    IPADAIR("ipadair", "iPad Air"), 
    IPADSIMULATOR("ipadsim", "iPad Simulator"),
    IPADRETINA("ipadretina", "iPad Retina");
    
    private String shortName;
    private String saucelabsName;

    private Tablet(String shortName, String saucelabsName) {
        this.shortName = shortName;
        this.saucelabsName = saucelabsName;
    }

    /**
     * Get Short name of the type as been used by Maven POM
     * 
     * @return String
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Get Saucelabs name of the type as been used by SauceLabs onDemand
     * 
     * @return String
     */
    public String getSaucelabsName() {
        return saucelabsName;
    }

    /**
     * Check if a given String is an instance of the type collection Tablet and deliver the respective type
     * 
     * @param t
     *            - the String to be checked
     * @return {@link Tablet}
     */
    public static Tablet lookup(String t) {
        for (Tablet tablet : values())
            if (tablet.getSaucelabsName().equalsIgnoreCase(t) || tablet.name().equalsIgnoreCase(t)
                    || tablet.getShortName().equalsIgnoreCase(t)) return tablet;
        return null;
    }
}