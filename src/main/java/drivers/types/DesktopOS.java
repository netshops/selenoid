package drivers.types;

/**
 * Available operating systems provided by sauce labs
 * 
 * @author Philip Taddey
 */
public enum DesktopOS {
    WINDOWS_XP("xp", "Windows XP"), 
    WINDOWS_7("win7", "Windows 7"), 
    WINDOWS_8("win8", "Windows 8"), 
    WINDOWS_8_1("win81", "Windows 8.1"), 
    WINDOWS_10("win10", "Windows 10"), 
    MAC_OS_X_MAVERICKS("mac109", "OS X 10.9"), 
    MAC_OS_X_YOSEMITE("mac1010", "OS X 10.10"),
    MAC_OS_X_ELCAPITAN("mac1011", "OS X 10.11"); 

    private String shortName;
    private String saucelabsName;

    private DesktopOS(String shortName, String saucelabsName) {
        this.shortName = shortName;
        this.saucelabsName = saucelabsName;
    }

    /**
     * Get the short name of the type as it is used by Maven
     * 
     * @return String
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Get the saucelabs name of the type as it is used by SauceLabs
     * 
     * @return String
     */
    public String getSauceLabsName() {
        return saucelabsName;
    }

    /**
     * Check if a given String is an expression of the type parsing against either short name and name.
     * 
     * @param s
     *            - The String to be checked
     * @return boolean
     */
    public boolean isPreciseInstanceOf(String s) {
        if (s.equalsIgnoreCase(this.getSauceLabsName())) return true;
        if (s.equalsIgnoreCase(this.getShortName())) return true;
        if (s.equalsIgnoreCase(this.name())) return true;
        return false;
    }

    /**
     * Looking for a type by a given String. If not found it will deliver a null value.
     * 
     * @param s
     *            - given search parameter
     * @return DesktopOS
     */
    public static DesktopOS lookup(String s) {
        for (DesktopOS os : DesktopOS.values())
            if (os.name().equalsIgnoreCase(s) || os.getSauceLabsName().equalsIgnoreCase(s)
                    || os.getShortName().equalsIgnoreCase(s)) return os;
        return null;
    }
}
