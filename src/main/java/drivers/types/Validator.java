package drivers.types;

/**
 * Validation of given environment settings
 * 
 * @author Philip Taddey
 */
public class Validator {
    public static boolean validateBrowserCombination(DesktopOS os, DesktopBrowser browser) {
        if (os == null || browser == null) return false;
        if (browser == DesktopBrowser.CHROME || browser == DesktopBrowser.FIREFOX) return true;

        switch (os) {
            case MAC_OS_X_MAVERICKS:
            case MAC_OS_X_YOSEMITE:
            case MAC_OS_X_ELCAPITAN:
                if (browser == DesktopBrowser.SAFARI) return true;
                if (browser == DesktopBrowser.INTERNET_EXPLORER) return false;
                if (browser == DesktopBrowser.MSEDGE) return false;
            case WINDOWS_7:
            case WINDOWS_8:
            case WINDOWS_8_1:
            case WINDOWS_XP:
                if (browser == DesktopBrowser.INTERNET_EXPLORER) return true;
                if (browser == DesktopBrowser.SAFARI) return false;
                if (browser == DesktopBrowser.MSEDGE) return false;
            case WINDOWS_10:
                if (browser == DesktopBrowser.MSEDGE) return true;
                if (browser == DesktopBrowser.SAFARI) return false;
        }
        return false;
    }
    
    /**
     * Valid values for Windows XP and Windows 7 are:
     * 800x600* 1024x768* 1052x864* 1152x864* 1280x800* 1280x960* 1280x1024* 1400x1050* 
     * 1440x900* 1600x1200* 1680x1050* 1920x1200* 2560x1600*
     * 
     * Valid values for OS X 10.8 are:
     * 1024x768* 1152x864* 1152x900* 1280x800* 1280x1024* 1376x1032* 1400x1050* 
     * 1600x1200* 1680x1050* 1920x1200*
     * 
     * Valid values for Windows 8 and 8.1 are:
     * 800x600* 1024x768* 1280x1024*
     * 
     * @param os
     * @param screenresolution
     * @return
     */
    public static boolean validateScreenResolution(DesktopOS os, ScreenResolution screenresolution) {
        if (os == null || screenresolution == null) return false;

        switch (screenresolution) {
        case SCREEN_800X600:
        	if (os == DesktopOS.WINDOWS_7) return true;
            if (os == DesktopOS.WINDOWS_XP) return true;
            if (os == DesktopOS.WINDOWS_8) return true;
            if (os == DesktopOS.WINDOWS_8_1) return true;
            if (os == DesktopOS.WINDOWS_10) return true;
        case SCREEN_1024X768:
        case SCREEN_1280X1024:
        	if (os == DesktopOS.WINDOWS_7) return true;
            if (os == DesktopOS.WINDOWS_XP) return true;
            if (os == DesktopOS.WINDOWS_8) return true;
            if (os == DesktopOS.WINDOWS_8_1) return true;
            if (os == DesktopOS.WINDOWS_10) return true;
            if (os == DesktopOS.MAC_OS_X_MAVERICKS) return true;
            if (os == DesktopOS.MAC_OS_X_YOSEMITE) return true;
            if (os == DesktopOS.MAC_OS_X_ELCAPITAN) return true;
        case SCREEN_1152X900:
        case SCREEN_1376X1032:
            if (os == DesktopOS.MAC_OS_X_MAVERICKS) return true;
            if (os == DesktopOS.MAC_OS_X_YOSEMITE) return true;
            if (os == DesktopOS.MAC_OS_X_ELCAPITAN) return true;
        case SCREEN_1052X864:
        case SCREEN_1280X960:
        case SCREEN_1440X900:
        case SCREEN_2560X1600:;
         	if (os == DesktopOS.WINDOWS_7) return true;
            if (os == DesktopOS.WINDOWS_XP) return true;
        case SCREEN_1152X864:
        case SCREEN_1280X800:
        case SCREEN_1400X1050:
        case SCREEN_1600X1200:
        case SCREEN_1680X1050:
        case SCREEN_1920X1200:
         	if (os == DesktopOS.WINDOWS_7) return true;
            if (os == DesktopOS.WINDOWS_XP) return true;
            if (os == DesktopOS.MAC_OS_X_MAVERICKS) return true;
            if (os == DesktopOS.MAC_OS_X_YOSEMITE) return true;
            if (os == DesktopOS.MAC_OS_X_ELCAPITAN) return true;
        }
        return false;
    }
}