package drivers.types;

/**
 * All desktop browsers that are provided by SauceLabs for automated testing
 * 
 * @author Philip Taddey
 */
public enum DesktopBrowser {
    CHROME("chrome"), 
    FIREFOX("firefox"), 
    INTERNET_EXPLORER("ie"), 
    MSEDGE("edge"), 
    SAFARI("safari");

    private String shortName;

    private DesktopBrowser(String shortName) {
        this.shortName = shortName;
    }

    /**
     * Get the short name of the browser needed to identify the correct browser for the execution projects. This name is
     * used in the Maven POM for Dclient parameter.
     * 
     * @return
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Check if a given String is member of this enumeration checking against either short name and name.
     * 
     * @param s
     *            - the String to be checked against enumeration
     * @return boolean
     */
    public boolean isInstanceOf(String s) {
        if (s == null) return false;
        for (DesktopBrowser b : DesktopBrowser.values())
            if (s.equalsIgnoreCase(b.getShortName())) return true;
        for (DesktopBrowser b : DesktopBrowser.values())
            if (s.equalsIgnoreCase(b.name())) return true;
        return false;
    }

    /**
     * Check if a given String is member of this enumeration instance checking against either short name and name.
     * 
     * @param s
     *            - the String to be checked against enumeration
     * @return boolean
     */
    public boolean isPreciseInstanceOf(String s) {
        if (s == null) return false;
        if (s.equalsIgnoreCase(this.getShortName())) return true;
        if (s.equalsIgnoreCase(this.name())) return true;
        return false;
    }

    /**
     * Looking up a type for a given String. If String cannot be found value is null.
     * 
     * @param s
     *            - the search parameter
     * @return DesktopBrowser
     */
    public static DesktopBrowser lookup(String s) {
        for (DesktopBrowser b : DesktopBrowser.values())
            if (b.name().equalsIgnoreCase(s) || b.getShortName().equalsIgnoreCase(s)) return b;
        return null;
    }
}