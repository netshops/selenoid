package drivers.types;

/**
 * The screen resolutions which can be executed with Sauce Labs
 * 
 * @author Philip Taddey
 */
public enum ScreenResolution {
	SCREEN_800X600("800x600"),
	SCREEN_1024X768("1024x768"),
	SCREEN_1280X1024("1280x1024"),
	SCREEN_1152X900("1152x900"),
	SCREEN_1376X1032("1376x1032"),
	SCREEN_1052X864("1052x864"),
	SCREEN_1280X960("1280x960"),
	SCREEN_1440X900("1440x900"),
	SCREEN_2560X1600("2560x1600"),
	SCREEN_1152X864("1152x864"),
	SCREEN_1280X800("1280x800"),
	SCREEN_1400X1050("1400x1050"),
	SCREEN_1600X1200("1600x1200"),
	SCREEN_1680X1050("1680x1050"),
	SCREEN_1920X1200("1920x1200");
	
	private String shortName;

    private ScreenResolution(String shortName) {
        this.shortName = shortName;
    }

    /**
     * Get the short name of the type as it is used by Maven
     * 
     * @return String
     */
    public String getShortName() {
        return shortName;
    }
    
    /**
     * Check if a given String is member of this enumeration checking against either short name and name.
     * 
     * @param s
     *            - the String to be checked against enumeration
     * @return boolean
     */
    public boolean isInstanceOf(String s) {
        if (s == null) return false;
        for (ScreenResolution sr : ScreenResolution.values())
            if (s.equalsIgnoreCase(sr.getShortName())) return true;
        for (ScreenResolution sr : ScreenResolution.values())
            if (s.equalsIgnoreCase(sr.name())) return true;
        return false;
    }

    /**
     * Check if a given String is member of this enumeration instance checking against either short name and name.
     * 
     * @param s
     *            - the String to be checked against enumeration
     * @return boolean
     */
    public boolean isPreciseInstanceOf(String s) {
        if (s == null) return false;
        if (s.equalsIgnoreCase(this.getShortName())) return true;
        if (s.equalsIgnoreCase(this.name())) return true;
        return false;
    }

    /**
     * Looking up a type for a given String. If String cannot be found value is null.
     * 
     * @param s
     *            - the search parameter
     * @return ScreenResolution
     */
    public static ScreenResolution lookup(String s) {
        for (ScreenResolution sr : ScreenResolution.values())
            if (sr.name().equalsIgnoreCase(s) || sr.getShortName().equalsIgnoreCase(s)) return sr;
        return null;
    }
}