package drivers.pool;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import core.exceptions.DriverFactoryException;
import core.models.Environment;
import core.settings.Settings;
import drivers.types.DesktopBrowser;
import drivers.types.DesktopOS;
import drivers.types.Mobile;
import drivers.types.ScreenResolution;
import drivers.types.Tablet;

/**
 * Driver Setup for test execution
 * 
 * @author Philip Taddey
 */
public class SauceDriverPool {
    /** Logging instance for CapabilityPool */
    private static Logger log = LogManager.getLogger(SauceDriverPool.class);
    
    /**
     * Setup local or remote driver
     * @throws IOException 
     */
    public static WebDriver getDriver(Environment env) throws DriverFactoryException, IOException {
        if (env == null) throw new DriverFactoryException("Missing Environment.");
        WebDriver driver;
        if (env.getTablet() != null) {
        	driver = createTablet(env);
        } else if (env.getMobile() != null) {
        	driver = createMobile(env);
        } else if (env.isLocal()) {
            driver = createLocal(env);
            driver.manage().window().setPosition(new Point(0,0));
            driver.manage().window().setSize(new Dimension(1280,1024));
        } else driver = createRemote(env);
        driver.manage().timeouts().implicitlyWait(env.getImplicitWait(), TimeUnit.SECONDS);
        return driver;
    }

    /**
     * Setup remote driver for desktop
     * @throws IOException 
     */
    public static WebDriver createRemote(Environment env) {
    	if (env.getOs() == null) env.setOs(DesktopOS.lookup(Settings.DEFAULT_OS));
        if (env.getBrowser() == null) env.setBrowser(DesktopBrowser.lookup(Settings.DEFAULT_BROWSER));
        if (env.getResolution() == null) env.setResolution(ScreenResolution.lookup(Settings.SCREEN_RESOLUTION));
        DesiredCapabilities caps = null;
        try {
            caps = SauceCapabilityPool.setupDesktopBrowser(env);
        } catch (DriverFactoryException e) {
            log.error("Environment is not allowed: " + env.toString() + "\nSetup will be using default.\n"
                    + e.getMessage());
            Environment environment = new Environment(DesktopBrowser.lookup(Settings.DEFAULT_BROWSER),
                    DesktopOS.lookup(Settings.DEFAULT_OS), Tablet.lookup(Settings.DEFAULT_TABLET), Mobile.lookup(Settings.DEFAULT_DEVICE),
                    null, null, ScreenResolution.lookup(Settings.SCREEN_RESOLUTION), Settings.IMPLICIT_WAIT, env.getSuite(), env.getJob(), env.getBuild(),
                    env.getWebsite(), null, env.isLocal(), env.isJira(), env.isTestrail(), null);
            createRemote(environment);
        }
        try {
            return new RemoteWebDriver(new URL(String.format(Settings.SAUCE_LABS_URL, Settings.SAUCE_LABS_USER,
                    Settings.SAUCE_LABS_TOKEN)), caps);
        } catch (MalformedURLException e) {
            log.error("Cannot create remote web driver. Try to use environment " + env.toString() + "\n"
                    + e.getMessage());
            return createRemote(new Environment(DesktopBrowser.CHROME, DesktopOS.WINDOWS_7, null, null, null, null,
            		env.getResolution(), env.getImplicitWait(), env.getSuite(), env.getJob(), env.getBuild(),
                    env.getWebsite(), null, false, env.isJira(), env.isTestrail(), null));
        }
    }

    /**
     * Setup local driver
     */
    public static WebDriver createLocal(Environment env) {
        DesiredCapabilities caps = SauceCapabilityPool.setupLocal(env.getBrowser());
        switch (env.getBrowser()) {
            case FIREFOX:
                return new FirefoxDriver(caps);
            case SAFARI:
                return new SafariDriver(caps);
            case CHROME:
            default:
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                return new ChromeDriver(caps);
        }
    }

    /**
     * Setup tablet driver
     */
    public static WebDriver createTablet(Environment env) throws MalformedURLException {
        Tablet t = env.getTablet();
        URL url = new URL(String.format(Settings.SAUCE_LABS_URL, Settings.SAUCE_LABS_USER,
                Settings.SAUCE_LABS_TOKEN));
        switch (t) {
            case GOOGLE_NEXUS_7_HD:
                return new RemoteWebDriver(url, SauceCapabilityPool.setupNextGenAndroidMobile(t.getSaucelabsName(), env));
            case IPAD2:
            case IPADAIR:
            case IPADSIMULATOR:
            case IPADRETINA:
                return new RemoteWebDriver(url, SauceCapabilityPool.setupIOs(t.getSaucelabsName(), env));
            case SAMSUNG_NOTE_10:
            case SAMSUNG_TAB_3:
            default:
                return new RemoteWebDriver(url, SauceCapabilityPool.setupAndroidMobile(t.getSaucelabsName(), env));
        }
    }
    
    /**
     * Setup mobile driver
     */
    public static WebDriver createMobile(Environment env) throws MalformedURLException {
        Mobile d = env.getMobile();
        URL url = new URL(String.format(Settings.SAUCE_LABS_URL, Settings.SAUCE_LABS_USER,
                Settings.SAUCE_LABS_TOKEN));
        switch (d) {
        	case IPHONE_SIMULATOR:
        	case IPHONE6PLUS:
        	case IPHONE6:
        	case IPHONE5S:
        	case IPHONE5:
        	case IPHONE4S:
	        	return new RemoteWebDriver(url, SauceCapabilityPool.setupIOs(d.getSaucelabsName(), env));
	        case SAMSUNG_GALAXY_S2:
	            return new RemoteWebDriver(url, SauceCapabilityPool.setupAndroidMobile(d.getSaucelabsName(), env));
	        case SAMSUNG_GALAXY_S3:
	        case SAMSUNG_GALAXY_S4:
	            return new RemoteWebDriver(url, SauceCapabilityPool.setupNextGenAndroidMobile(d.getSaucelabsName(), env));
	        default:
	        	return new RemoteWebDriver(url, SauceCapabilityPool.setupIOs(d.getSaucelabsName(), env));
        }
    }
}