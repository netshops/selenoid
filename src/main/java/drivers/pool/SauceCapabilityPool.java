package drivers.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;

import core.exceptions.DriverFactoryException;
import core.models.Environment;
import core.settings.Settings;
import drivers.types.DesktopBrowser;
import drivers.types.DesktopOS;
import drivers.types.ScreenResolution;
import drivers.types.Validator;

/**
 * All Capabilities needed by the system to communicate with SauceLabs
 * 
 * @author Philip Taddey
 */
public class SauceCapabilityPool {
    /** Logging instance for CapabilityPool */
    private static Logger log = LogManager.getLogger(SauceCapabilityPool.class);
    private static String loggingMessageStep = "Created set of capabilities for %s: %s";
    
    /**
     * Android Linux
     * Version: 4.3 or lower
     */
    public static DesiredCapabilities setupAndroidMobile(String name, Environment env) {
        DesiredCapabilities caps = DesiredCapabilities.android();
        caps.setCapability("deviceName", name);
        caps.setCapability("deviceOrientation", env.getOrientation());
        caps.setCapability("browserName", "Android");
        caps.setCapability("platform", "Linux");
        caps.setCapability("version", env.getPlatformVersion());
        setSauceLabsRecipeMobile(caps, env);
        log.debug("Set of Capabilities: " + caps.toString());
        return caps;
    }

    /**
     * Android
     * Version: 4.4 or higher
     */  
    public static DesiredCapabilities setupNextGenAndroidMobile(String name, Environment env) {
        DesiredCapabilities caps = DesiredCapabilities.android();
        caps.setCapability("appiumVersion", Settings.APPIUM_VERSION);
        caps.setCapability("deviceName", name);
        caps.setCapability("deviceOrientation", env.getOrientation());
        caps.setCapability("browserName", "Chrome");
        caps.setCapability("platformVersion", env.getPlatformVersion());
        caps.setCapability("platformName", "Android");
        setSauceLabsRecipeMobile(caps, env);
        log.debug("Set of Capabilities: " + caps.toString());
        return caps;
    }

    /**
     * iOs (iPhone and iPad Devices)
     */
    public static DesiredCapabilities setupIOs(String name, Environment env) {
        DesiredCapabilities caps = DesiredCapabilities.iphone();
        caps.setCapability("appiumVersion", Settings.APPIUM_VERSION);
        caps.setCapability("deviceName", name);
        caps.setCapability("deviceOrientation", env.getOrientation());
        caps.setCapability("platformVersion", env.getPlatformVersion());
        caps.setCapability("platformName", "iOS");
        caps.setCapability("browserName", "Safari");
        setSauceLabsRecipeMobile(caps, env);
        log.debug("Set of Capabilities: " + caps.toString());
        return caps;
    }

    /**
     * Desktop Browser
     */
    public static DesiredCapabilities setupDesktopBrowser(Environment env) throws DriverFactoryException {
        if (env == null) throw new DriverFactoryException("Cannot create an instance without envuration");
        if (env.getOs() == null) env.setOs(DesktopOS.lookup(Settings.DEFAULT_OS));
        if (env.getBrowser() == null) env.setBrowser(DesktopBrowser.lookup(Settings.DEFAULT_BROWSER));
        /* Validate os and browser combination */
        if (!Validator.validateBrowserCombination(env.getOs(), env.getBrowser())) {
            log.warn("Combination of given os/browser " + env.getOs().name() + "/" + env.getBrowser().name()
                    + " is not valid. Setting " + Settings.DEFAULT_OS + "/" + Settings.DEFAULT_BROWSER
                    + " as default.");
            env.setOs(DesktopOS.lookup(Settings.DEFAULT_OS));
            env.setBrowser(DesktopBrowser.lookup(Settings.DEFAULT_BROWSER));
        }
        /* Validate screen resolution */
        if (!Validator.validateScreenResolution(env.getOs(), env.getResolution())) {
            log.warn("Screen resolution of given os/resoulution " + env.getOs().name() + "/" + env.getResolution().name()
                    + " is not valid. Setting " + Settings.SCREEN_RESOLUTION + " as default.");
            env.setResolution(ScreenResolution.lookup(Settings.SCREEN_RESOLUTION));
        }
        DesiredCapabilities caps = getDesktopBrowserCapabilities(env.getBrowser());
        if (null != env.getBrowserVersion() && !env.getBrowserVersion().isEmpty())
            caps.setCapability("version", env.getBrowserVersion());
        getOSCapabilities(caps, env.getOs().getSauceLabsName());
        setSauceLabsRecipe(caps, env);
        log.debug(String.format(loggingMessageStep, env.getBrowser().name(), caps.toString()));
        return caps;
    }

    /**
     * Local Testing
     */
    public static DesiredCapabilities setupLocal(DesktopBrowser browser) {
        if (browser == null) browser = DesktopBrowser.lookup(Settings.DEFAULT_BROWSER);
        DesiredCapabilities caps = getDesktopBrowserCapabilities(browser);
        return caps;
    }

    /**
     * Setup Browser Capabilities
     */
    private static DesiredCapabilities getDesktopBrowserCapabilities(DesktopBrowser browser) {
        DesiredCapabilities caps;
        switch (browser) {
            case FIREFOX:
                caps = DesiredCapabilities.firefox();
                break;
            case INTERNET_EXPLORER:
                caps = DesiredCapabilities.internetExplorer();
                break;
            case MSEDGE:
                caps = DesiredCapabilities.edge();
                break;
            case SAFARI:
                caps = DesiredCapabilities.safari();
                break;
            case CHROME:
            default:
            	caps = DesiredCapabilities.chrome();
                break;
        }
        return caps;
    }

    /**
     * Setup Operating System
     */   
    private static void getOSCapabilities(DesiredCapabilities caps, String os) {
        if (DesktopOS.WINDOWS_XP.isPreciseInstanceOf(os))
            caps.setCapability("platform", "Windows XP");
        else if (DesktopOS.WINDOWS_8.isPreciseInstanceOf(os))
            caps.setCapability("platform", "Windows 8");
        else if (DesktopOS.WINDOWS_8_1.isPreciseInstanceOf(os))
            caps.setCapability("platform", "Windows 8.1");
        else if (DesktopOS.WINDOWS_10.isPreciseInstanceOf(os))
            caps.setCapability("platform", "Windows 10");
        else if (DesktopOS.MAC_OS_X_ELCAPITAN.isPreciseInstanceOf(os))
            caps.setCapability("platform", "OS X 10.11");
        else if (DesktopOS.MAC_OS_X_YOSEMITE.isPreciseInstanceOf(os))
            caps.setCapability("platform", "OS X 10.10");
        else if (DesktopOS.MAC_OS_X_MAVERICKS.isPreciseInstanceOf(os))
            caps.setCapability("platform", "OS X 10.9");
        else caps.setCapability("platform", "Windows 7");
    }

    /**
     * Sauce Labs Settings Non Mobile
     */    
    private static void setSauceLabsRecipe(DesiredCapabilities caps, Environment env) {
    	// Set screen resolution
        caps.setCapability("screenResolution", env.getResolution().getShortName());
        // Set selenium version
        caps.setCapability("seleniumVersion", Settings.SELENIUM_VERSION);
        caps.setCapability("name", env.getJob());
        caps.setCapability("build", env.getBuild());

        // Set capability respective to seleniumVersion
        if (caps.getCapability("seleniumVersion").equals(Settings.SELENIUM_VERSION)) {
            caps.setCapability("acceptSslCerts", true);
            caps.setCapability("cssSelectorsEnabled", true);
        } else {
            caps.setCapability("acceptSslCerts", "true");
            caps.setCapability("cssSelectorsEnabled", "true");
        }
        caps.setCapability("disablePopupHandler", Settings.POPUP_HANDLE);
        caps.setCapability("maxDuration", Settings.MAX_DURATION);
        caps.setCapability("commandTimeout", Settings.COMMAND_TIMEOUT);
    }
    
    /**
     * Sauce Labs Settings Mobile
     */   
    private static void setSauceLabsRecipeMobile(DesiredCapabilities caps, Environment env) {
        caps.setCapability("name", env.getJob());
        caps.setCapability("build", env.getBuild());
        caps.setCapability("maxDuration", Settings.MAX_DURATION);
        caps.setCapability("commandTimeout", Settings.COMMAND_TIMEOUT);
    }
}