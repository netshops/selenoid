package drivers.pool;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import core.exceptions.DriverFactoryException;
import core.models.Environment;
import core.settings.Settings;

/**
 * Remote webdriver for inhouse selenium server
 * 
 * @author Philip Taddey
 */
public class RemoteDriver {
    static WebDriver driver;
    
    /**
     * Setup local or remote driver
     * @throws IOException 
     */
    public static WebDriver getDriver(Environment env) throws DriverFactoryException, IOException {
        if (env == null) throw new DriverFactoryException("Missing Environment.");
        if (env.isLocal()) {
            driver = createLocal(env);
            driver.manage().window().setPosition(new Point(0,0));
            driver.manage().window().setSize(new Dimension(1280,1024));
        } else driver = setupRemote(env);
        driver.manage().timeouts().implicitlyWait(env.getImplicitWait(), TimeUnit.SECONDS);
        driver.manage().window().setSize(new Dimension(1280,1024));
        return driver;
    }
    
    /**
     * Setup remote driver for specific environments
     * @param env
     * @return Driver
     * @throws MalformedURLException
     */
    public static WebDriver setupRemote(Environment env) throws MalformedURLException {
        DesiredCapabilities caps;
        switch (env.getBrowser()) {
            case FIREFOX:
                caps = DesiredCapabilities.firefox();
                break;
            case INTERNET_EXPLORER:
                caps = DesiredCapabilities.internetExplorer();
                caps.setCapability("version", env.getBrowserVersion());
                caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true); 
                break;
            case MSEDGE:
                caps = DesiredCapabilities.edge();
                break;
            case SAFARI:
                caps = DesiredCapabilities.safari();
                break;
            case CHROME:
            default:
                caps = DesiredCapabilities.chrome();
                break;
        }
        driver = new RemoteWebDriver(new URL(Settings.REMOTE_ADDRESS), caps);
        return driver;
    }
    
    /**
     * Setup local driver
     */
    public static WebDriver createLocal(Environment env) {
        DesiredCapabilities caps;
        switch (env.getBrowser()) {
            case FIREFOX:
                caps = DesiredCapabilities.firefox();
                return new FirefoxDriver(caps);
            case SAFARI:
                caps = DesiredCapabilities.safari();
                return new SafariDriver(caps);
            case CHROME:
            default:
                caps = DesiredCapabilities.chrome();
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                return new ChromeDriver(caps);
        }
    }   
}