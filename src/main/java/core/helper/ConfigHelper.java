package core.helper;

import java.util.Properties;

import core.models.Environment;
import core.models.Visual;
import drivers.types.DesktopBrowser;
import drivers.types.DesktopOS;
import drivers.types.Mobile;
import drivers.types.ScreenResolution;
import drivers.types.Tablet;

/**
 * Setup for the configuration which is under test.
 * 
 * @author Philip Taddey
 */
public class ConfigHelper {
    
    /**
     * Get all environment information from maven for text execution
     * 
     * @param maven
     * @param className
     * @param jobName
     * @return Environment Setup
     */
	public static Environment setupEnvironment(Properties maven, String className, String jobName) {
        return new Environment(DesktopBrowser.lookup(maven.getProperty("browser")), DesktopOS.lookup(maven
                .getProperty("os")), Tablet.lookup(maven.getProperty("tablet")), Mobile.lookup(maven.getProperty("mobile")), maven.getProperty("platform.version"), maven.getProperty("orientation"),
                ScreenResolution.lookup(maven.getProperty("screen")), Integer.parseInt(maven.getProperty("implicit.wait")), className, jobName, maven.getProperty("project") + "::"
                + maven.getProperty("project.build"), maven.getProperty("website"),
                maven.getProperty("browser.version"), maven.getProperty("local").equals("true") ? true : false, maven.getProperty("jira").equals("true") ? true : false,
                maven.getProperty("testrail").equals("true") ? true : false, maven.getProperty("testrail_id"));
    }
	
	/**
	 * Get all environment information from maven for visual regression test execution
	 * @param maven
	 * @param section
	 * @return Environment 
	 */
    public static Visual setupVisualConfig(Properties maven, String section) {
        return new Visual(maven.getProperty("browser"), maven.getProperty("os"), 
                Tablet.lookup(maven.getProperty("tablet")), Mobile.lookup(maven.getProperty("mobile")), section, 
                maven.getProperty("browser.version"), maven.getProperty("screen"),
                maven.getProperty("orientation"));
    }
    
}