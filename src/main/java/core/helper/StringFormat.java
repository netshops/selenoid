package core.helper;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Helper for clean up specific strings
 */
public class StringFormat {

    public static String cleanFromSpecialSigns(String s) {
        return s.replace("Ä", "Ae").replace("ä", "ae").replace("Ö", "Oe").replace("ö", "oe").replace("Ü", "Ue")
                .replace("ü", "ue").replace("ß", "ss");
    }

    public static String cleanPrice(String s) {
        return s.replace(",", ".").replace("€", "").replace(" ", "").replace("AB", "").replace("Ab", "")
                .replace("ab", "");
    }
    
    public static String getSinglePrice(String s) {
        return s.replace("€", "").replace(" ", "");
    }

    public static int convertPriceToInteger(String s) {
        return Integer.parseInt(cleanPrice(s.replace(",", "").replace(".", "")));
    }
    
    public static BigDecimal getDoublePrice(String price) {
    	double priceDouble = Double.parseDouble(cleanPrice(price));
	    BigDecimal priceRounded = new BigDecimal( priceDouble );
	    priceRounded = priceRounded.setScale( 2, BigDecimal.ROUND_HALF_UP );
	    return priceRounded;
	}
    
	public static String getStringUntilChars(String s, String chars) {
		if (s.contains("...")) return s.substring(0, s.indexOf(chars));
		return s;
	}
	
	public static String getStringInBrackets(String s, String bracket1, String bracket2) {
		return s.substring(s.indexOf(bracket1)+1,s.indexOf(bracket2));
	}
	
	public static String getCurrentDate(String format) {
	    DateFormat dateFormat = new SimpleDateFormat(format);
	    Date date = new Date();
	    return dateFormat.format(date);
	}
}