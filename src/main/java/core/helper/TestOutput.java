package core.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Reporter;

/**
 * Logging for test execution
 * 
 * @author Philip Taddey
 */
public class TestOutput {
    private static Logger log = LogManager.getLogger(TestOutput.class);
    
    /**
     * Logs the specific test case 
     * 
     * @param section Class Name
     * @param test Method Name
     * @param reason Custom String
     */
	public static void print(String section, String test, String reason) {
    	log("=======================================================");
		log(" TEST: " + section + " > " + test + " > " + reason);
    	log("=======================================================");
		log("\n" + section + " > " + test + " > " + reason + "\n");
	}
	
	/**
	 * Log trace
	 * @param message
	 */
	public static void log(String message) {
	    log.trace(message);
	    Reporter.log(message + "\n");
	}
}