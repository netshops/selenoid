package core.runner;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import core.helper.TestOutput;
import core.settings.Settings;

/**
 * Provides all needed selenium webdriver commands, wait methods and assertions.
 * 
 * @author Philip Taddey
 */
public class Runner {
    /* Set Highlighting on / off */
    private boolean highlight = false;
    /* Logging */
	public Logger log = LogManager.getLogger(Runner.class);
    /* WebDriver Initialization */
    private WebDriver driver;
    
    /* Constructor */
    public Runner(WebDriver driver) {
    	super();
    	this.driver = driver;
    }
    
    /**
     * Highlight element
     * 
     * Highlights element in specified color
     */
    public void highlightElement(By element, WebDriver driver) {
        scrollTo(element);
        String originalStyle = driver.findElement(element).getAttribute("style");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow;');", driver.findElement(element));
        try {
            Thread.sleep(1000);
        } 
        catch (InterruptedException e) {}
        js.executeScript("arguments[0].setAttribute('style', '" + originalStyle + "');", driver.findElement(element));
    }
    
    /**
     * WebDriver Commands
     */
	public boolean canSee(By element) {
		TestOutput.log("I can see " + element);
		if (highlight == true) highlightElement(element, driver);
		return driver.findElements(element).size() > 0 ? true : false;
	}
    
    public boolean cannotSee(By element) {
        TestOutput.log("I cannot see " + element);
        if (highlight == true) highlightElement(element, driver);
        return driver.findElements(element).size() > 0 ? false : true;
    }
	
	public void check(By element) {
		TestOutput.log("I check " + element);
		if (highlight == true) highlightElement(element, driver);
		driver.findElement(element).click();
	}
	
	public void click(By element) {
		TestOutput.log("I click on " + element);
		if (highlight == true) highlightElement(element, driver);
		driver.findElement(element).click();
	}
	
	public void waitAndClick(By element) {
        TestOutput.log("I wait and click on " + element);
        new WebDriverWait(driver, Settings.IMPLICIT_WAIT).ignoring(NoSuchElementException.class)
        .ignoring(StaleElementReferenceException.class).ignoring(ElementNotVisibleException.class)
        .until(ExpectedConditions.elementToBeClickable(element)).click();	
    }
	
	public void customSelect(String optionClass, String option) {
        TestOutput.log("I custom select the option " + option + " from the dropdown");
        int amountOptions = this.getNumOfElements(By.cssSelector(optionClass));
        for (int pos = 1; pos <= amountOptions; pos++) {
            String sortOption = this.getText(By.cssSelector(optionClass + ":nth-child(" + pos + ")"));
            if (sortOption.equalsIgnoreCase(option)) {
                this.click(By.cssSelector(optionClass + ":nth-child(" + pos + ")"));
                return;
            }
        }
        throw new NoSuchElementException("The dropdown does not contain the option " + option
                + " and therefore it is not possible to select it.");
    }
	
	public void jsClick(By element) {
		TestOutput.log("I click via JavaScript on " + element);
        if (driver instanceof JavascriptExecutor) ((JavascriptExecutor) driver).executeScript("window.document.getElementById('"+ element +"').click()");
	}
	
    public void deleteAllCookies() {
        TestOutput.log("I delete all cookies");
        driver.manage().deleteAllCookies();
    }
	
	public void enterText(By element, String text) {
		TestOutput.log("I fill in " + text + " into " + element);
		if (highlight == true) highlightElement(element, driver);
        WebElement textField = new WebDriverWait(driver, Settings.IMPLICIT_WAIT).ignoring(NoSuchElementException.class)
                .ignoring(ElementNotVisibleException.class).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.visibilityOfElementLocated(element));
        textField.clear();
        textField.sendKeys(text);
	}
	
	public String getAttributeValueOf(By element, String attribute) {
	    if (highlight == true) highlightElement(element, driver);
	    WebElement attributeElement = new WebDriverWait(driver, Settings.IMPLICIT_WAIT)
           .ignoring(NoSuchElementException.class).ignoring(ElementNotVisibleException.class)
           .ignoring(StaleElementReferenceException.class)
           .until(ExpectedConditions.visibilityOfElementLocated(element));
	    String attributeValue = attributeElement.getAttribute(attribute);
	    TestOutput.log("I get " + attributeValue + " from " + element + "[" + attribute + "]");
	    return attributeValue;
	}
	
	public WebElement getElement(By element) {
		TestOutput.log("I get the webelement " + element);
		return driver.findElement(element);
	}
	
    public int getNumOfElements(By element) {
        if (highlight == true) highlightElement(element, driver);
        int size = 0;
    	TestOutput.log("I see " +  (size = driver.findElements(element).size()) + " elements for " + element);
    	return size;
    }
    
    public String getPageTitle() {
    	String pageTitle;
        TestOutput.log("I get the page title " + (pageTitle = driver.getTitle()));
        return pageTitle;    
    }
    
    public String getPageSource() {
        String pageSource = driver.getPageSource();
        TestOutput.log("I get the page source");
        return pageSource;    
    }
    
    public String getSelectedOption(By element) {
    	  TestOutput.log("I get the selected option of " + element);
          WebElement select = new WebDriverWait(driver, Settings.IMPLICIT_WAIT).ignoring(NoSuchElementException.class)
                  .ignoring(StaleElementReferenceException.class).ignoring(ElementNotVisibleException.class)
                  .until(ExpectedConditions.visibilityOfElementLocated(element));
          Select s = new Select(select);
          List<WebElement> options = s.getOptions();
          for (WebElement option : options)
              if (option.isSelected()) return option.getText();
          return "";
    }
    
    public String getText(By element) {
        if (highlight == true) highlightElement(element, driver);
    	String text = driver.findElement(element).getText();
        TestOutput.log("I get text '" + text + "' from " + element);
    	return text;
    }
    
    public void hover(By element) {
    	TestOutput.log("I hover the element " + element);
    	if (highlight == true) highlightElement(element, driver);
    	Actions actions = new Actions(driver);
    	WebElement el = driver.findElement(element);
    	actions.moveToElement(el).build().perform();
    }
    
    public void moveMouseTo(By element) {
    	TestOutput.log("I move mouse to " + element);
    	if (highlight == true) highlightElement(element, driver);
        Actions a = new Actions(driver);
        a.moveToElement(
                new WebDriverWait(driver, Settings.IMPLICIT_WAIT).ignoring(NoSuchElementException.class)
                        .ignoring(ElementNotVisibleException.class).ignoring(StaleElementReferenceException.class)
                        .until(ExpectedConditions.visibilityOfElementLocated(element))).build().perform();
    }
    
    public void moveMouseAway(By element) {
    	TestOutput.log("I move mouse away from " + element);
        Actions a = new Actions(driver);
        a.moveToElement(
                new WebDriverWait(driver, Settings.IMPLICIT_WAIT).ignoring(NoSuchElementException.class)
                .ignoring(ElementNotVisibleException.class).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.visibilityOfElementLocated(element)), 100, 100).build().perform();
    }
    
    public void maximizeWindow() {
        TestOutput.log("I maximize window");
        driver.manage().window().maximize();
    }

    public void navigateBack() {
        TestOutput.log("I navigate back");
        driver.navigate().back();
    }

    public void navigateForward() {
        TestOutput.log("I navigate forward");
        driver.navigate().forward();
    }
    
	public void openHomepage(String url) {
		TestOutput.log("I open the homepage " + url);
		driver.get(url);
	}
	
	public void openHomepageWithStatement(String url, By element, By link) {
        TestOutput.log("I open the homepage " + url + " and I accept the cookie statement");
        driver.get(url);
        this.acceptCookies(element, link);
    }
    
	public void pressEnter(By element) {
		TestOutput.log("I press ENTER");
		driver.findElement(element).sendKeys(Keys.ENTER);
	}
	
	public void resizeWindowTo(Dimension d) {
        TestOutput.log("I resize window to " + d.toString());
        driver.manage().window().setSize(d);
    }
	
    public void scrollTo(By element) {
        WebElement el = driver.findElement(element);
        Actions actions = new Actions(driver);
        TestOutput.log("I scroll to the element " + element);
        actions.moveToElement(el);
        actions.perform();    
    }
    
	public void scrollToTop() {
	    TestOutput.log("I scroll to the top of the page");
        if (driver instanceof JavascriptExecutor) ((JavascriptExecutor) driver).executeScript("window.scrollTo(500,0);");
	}
	
	public void scrollToBottom() {
	       TestOutput.log("I scroll to the bottom of the page");
	    if (driver instanceof JavascriptExecutor) ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight);");
	}
	
	public boolean see(By element) {
		TestOutput.log("I see " + element);
		if (highlight == true) highlightElement(element, driver);
		WebElement wait = new WebDriverWait(driver, Settings.IMPLICIT_WAIT).ignoring(NoSuchElementException.class)
	        .ignoring(StaleElementReferenceException.class).ignoring(ElementNotVisibleException.class)
            .until(ExpectedConditions.visibilityOfElementLocated(element));
		return wait.isDisplayed();
	}
	
	public void select(By element, String option) {
		TestOutput.log("I select the option " + option + " from the dropdown " + element);
		if (highlight == true) highlightElement(element, driver);
        WebElement select = new WebDriverWait(driver, Settings.IMPLICIT_WAIT)
        		.ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class)
        		.ignoring(ElementNotVisibleException.class)
                .until(ExpectedConditions.visibilityOfElementLocated(element));
        Select s = new Select(select);
        List<WebElement> options = s.getOptions();
        for (WebElement o : options) {
            if (o.getText().equalsIgnoreCase(option) || o.getAttribute("value").equalsIgnoreCase(option)) {
                o.click();
                return;
            }
        }
        throw new NoSuchElementException("The element " + element + " do not contain the option " + option
                + " and therefore it is not possible to select it.");
    }
	
	public void submit(By element) {
		TestOutput.log("I submit " + element);
		if (highlight == true) highlightElement(element, driver);
		driver.findElement(element).submit();
	}
	
    public void setFocusOn(String name) {
        TestOutput.log("I switch to iFrame by " + name);
        driver.switchTo().frame(name);
    }
	
    public void quit() {
        TestOutput.log("I terminate the browser session");
        driver.quit();
    }

    public void useJavaScript(String jsExecute) {
        TestOutput.log("I want to execute the JavaScript " + jsExecute);
        if (driver instanceof JavascriptExecutor) ((JavascriptExecutor) driver).executeScript(jsExecute);
    }

	/**
	 * Wait Commands
	 */
    public void waitFor(long seconds) {
        TestOutput.log("I force the site to wait for '" + seconds + "' seconds");
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
	public void waitForElementToChange(By element, int timeout) {
		new WebDriverWait(driver, timeout == 0 ? Settings.IMPLICIT_WAIT : timeout)
			.ignoring(NoSuchElementException.class).ignoring(ElementNotVisibleException.class)
			.pollingEvery(500L, TimeUnit.MILLISECONDS)
			.until(ExpectedConditions.visibilityOfElementLocated(element));
	}
	
    public void waitForPageReloaded(int timeout) {
        TestOutput.log("I wait for " + timeout + " seconds for the page reload");
    	new WebDriverWait(driver, timeout == 0 ? Settings.IMPLICIT_WAIT : timeout).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
		});
    }
    
    public void waitForElementToBeDisplayed(By element, int timeout) {
        TestOutput.log("I wait for " + timeout + " seconds for the visibility of " + element);
    	new WebDriverWait(driver, timeout == 0 ? Settings.IMPLICIT_WAIT : timeout)
            .ignoring(NoSuchElementException.class).ignoring(ElementNotVisibleException.class)
            .until(ExpectedConditions.visibilityOfElementLocated(element));
    }
    
    public void waitForElementToBeNotDisplayed(By element, int timeout) {
        TestOutput.log("I wait for " + timeout + " seconds for the invisibility of " + element);
        new WebDriverWait(driver, timeout == 0 ? Settings.IMPLICIT_WAIT : timeout)
            .ignoring(NoSuchElementException.class).ignoring(ElementNotVisibleException.class)
            .until(ExpectedConditions.not(ExpectedConditions.visibilityOfElementLocated(element)));
    }
    
    public void waitForAjaxRequest(long wait) {
        TestOutput.log("I want to wait for " + wait + " seconds for AJAX request to complete");
        new WebDriverWait(driver, (wait * 1000)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                return (Boolean) js.executeScript("return jQuery.active == 0");
            }
        });
    }
    
    /**
     * Assertions
     */
    public void assertEquals(String given, String expected) {
        TestOutput.log("I assert " + given + " is equal to " + expected);
        Assert.assertEquals(given, expected);
    }
    
    public void assertEqualsIgnoreCase(String given, String expected) {
        TestOutput.log("I assert " + given.toLowerCase() + " is equal to " + expected.toLowerCase());
        Assert.assertEquals(given.toLowerCase(), expected.toLowerCase());
    }

    public void assertContains(String haystack, String needle) {
        TestOutput.log("I assert " + haystack + " contains " + needle);
        Assert.assertTrue(haystack.contains(needle));
    }
    
    public void assertContainsIgnoreCase(String haystack, String needle) {
        TestOutput.log("I assert " + haystack.toLowerCase() + " contains " + needle.toLowerCase());
        Assert.assertTrue(haystack.toLowerCase().contains(needle.toLowerCase()));
    }
    
    public void assertLess(int given, int expected) {
        TestOutput.log("I assert " + given + " is less " + expected);
        Assert.assertTrue(given < expected);
    }

    public void assertLess(double given, double expected) {
        TestOutput.log("I assert " + given + " is less " + expected);
        Assert.assertTrue(given < expected);
    }
    
    public void assertLessOrEqual(int less, int expected) {
        TestOutput.log("I assert " + less + " is less or equal " + expected);
        Assert.assertTrue(less <= expected);
    }
    
    public void assertLessOrEqual(double less, double expected) {
        TestOutput.log("I assert " + less + " is less or equal " + expected);
        Assert.assertTrue(less <= expected);
    }

    public void assertGreaterOrEqual(int greater, int expected) {
        TestOutput.log("I assert " + greater + " is greater or equal " + expected);
        Assert.assertTrue(greater >= expected);
    }

    public void assertGreaterOrEqual(double greater, double expected) {
        TestOutput.log("I assert " + greater + " is greater or equal " + expected);
        Assert.assertTrue(greater >= expected);
    }
    
    public void assertGreater(int greater, int expected) {
        TestOutput.log("I assert " + greater + " is greater " + expected);
        Assert.assertTrue(greater > expected);
    }

    public void assertGreater(double greater, double expected) {
        TestOutput.log("I assert " + greater + " is greater " + expected);
        Assert.assertTrue(greater > expected);
    }

    public void assertInBetween(int less, int expected, int greater) {
        TestOutput.log("I assert " + less + " is less or equals and " + greater + " is greater or equal " + expected);
        Assert.assertTrue(less <= expected && greater >= expected);
    }
    
    public void assertInBetween(double less, double expected, double greater) {
        TestOutput.log("I assert " + less + " is less or equals and " + greater + " is greater or equal " + expected);
        Assert.assertTrue(less <= expected && greater >= expected);
    }

    public void assertNot(String given, String expected) {
        TestOutput.log("I assert " + given + " is not equal to " + expected);
        Assert.assertNotEquals(given, expected);
    }

    public void assertContainsNot(String given, String expected) {
        TestOutput.log("I assert " + given + " contains not " + expected);
        Assert.assertFalse(given.contains(expected));
    }

    public void assertEqual(int given, int expected) {
        TestOutput.log("I assert " + given + " is equal " + expected);
        Assert.assertTrue(given == expected);
    }

    public void assertEqual(double given, double expected) {
        TestOutput.log("I assert " + given + " is equal " + expected);
        Assert.assertTrue(given == expected);
    }
    
    public void assertEqual(BigDecimal given, BigDecimal expected) {
        TestOutput.log("I assert " + given + " is equal " + expected);
        Assert.assertTrue(given.equals(expected));
    }

    public void assertTrue(boolean bool) {
        TestOutput.log("I assert that " + bool + " is true");
        Assert.assertTrue(bool);
    }
    
    /**
     * URL Management
     */
    public void openUrl(String website, String url) {
    	TestOutput.log("I open " + website + url);
        driver.get(website + url);
    }
    
    public String getCurrentUrl() {
    	String url = driver.getCurrentUrl();
    	TestOutput.log("I get the current url " + url);
    	return url;
    }
 
    public String getParameterOfUrl(String url) {
        String param = url;
    	if (param.contains("?")) {
    		param = param.substring(param.lastIndexOf("?") + 1);
        }
    	TestOutput.log("I get the param " + param + " from " + url);
        return param;
    }

    public String getDocumentAnchorOfUrl(String url) {
    	String anchor = url;
     	if (anchor.contains("?")) {
     		anchor = anchor.substring(anchor.lastIndexOf("?") + 1);
         }
     	TestOutput.log("I get the param " + anchor + " from " + url);
        return anchor;
    }
    
    public void assertHttps() {
     	TestOutput.log("I assert that " + this.getCurrentUrl() + " contains https");
    	this.assertContains(this.getCurrentUrl(), "https");
    }
    
    public void refreshPage() {
        TestOutput.log("I refresh the page");
        driver.navigate().refresh();
    }
    
    /**
     * String Generator
     */
    public String createRegisterMail() {
        String mail = "testing+" + Double.toHexString(Math.random()) + "@selenoid.de";
        TestOutput.log("I create the new mail address " + mail + "for registration");
        return mail;
    }
    
    public String createString(int length) {
        TestOutput.log("I generate a new random string with length " + length);
        String str = RandomStringUtils.randomAlphabetic(length);
        return str;
    }
    
    /**
     * Cookie Layer
     */
    public void acceptCookies(By element, By link) {
        if (this.canSee(element)) {
            this.click(link);
        }
    }
    
    /**
     * Screenshot On Failure
     * 
     * @throws IOException 
     */
    public File takeScreenshot(String name) throws IOException {
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
        Date date = new Date();
        FileUtils.copyFile(source, new File("./target/test-failures/" + name + "_" + dateFormat.format(date) + ".png"));
        TestOutput.log("Failed: Screenshot attached!");
        toByteArray(source);
        return source;
    }
    
    public byte[] toByteArray(File name) throws IOException {
        return Files.readAllBytes(Paths.get(name.getPath())); 
    } 
    
    /**
     * Link Checker
     * 
     * @param link Link which should be checked
     * @return true (link is valid), false (link is broken)
     */
    public boolean assertLinkIsValid(String link){
        try {
           URL url = new URL(link);
           HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();
           httpURLConnect.setConnectTimeout(3000);
           httpURLConnect.connect();
           if(httpURLConnect.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND 
                   || httpURLConnect.getResponseCode() == HttpURLConnection.HTTP_GATEWAY_TIMEOUT 
                   || httpURLConnect.getResponseCode() == HttpURLConnection.HTTP_INTERNAL_ERROR ){
               log.trace(httpURLConnect.getResponseCode() + "Error: " + link + " - " + httpURLConnect.getResponseMessage()
                       + " - "+ HttpURLConnection.HTTP_NOT_FOUND);
               return false;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    } 
}