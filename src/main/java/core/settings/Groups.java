package core.settings;

/**
 * Groups are used for clustering the tests so that only
 * specific tests can be executed.
 * 
 * @author Philip Taddey
 */
public class Groups {
    /* Live Tests */
    public static final String LIVE = "live";
    /* Stage Tests */
    public static final String STAGE = "staging";
    /* Tests under Development */
    public static final String DEV = "develop";
    /* Tests under Development */
    public static final String IN_DEV = "indev";
    /* Visual Only Tests */
    public static final String VISUAL = "visual";
    /* Desktop Viewport Tests */
    public static final String DESKTOP = "desktop";
    /* Tablet Viewport Tests */
    public static final String TABLET = "tablet";
    /* Smartphone Viewport Tests */
    public static final String MOBILE = "mobile";
    /* Failed Tests */
    public static final String FAILED = "failed";
    /* Demo Tests */
    public static final String DEMO = "demo";
    /* Order Tests  */
    public static final String ORDER = "order";
    /* Monitoring Tests */
    public static final String MONITORING = "monitoring";
    /* Critical Business Path */
    public static final String CBP = "cbp";
    /* Full Tests */
    public static final String FULL = "full";
    /* Multilanguage Test */
    public static final String MULTILANG = "multilang";
    
}