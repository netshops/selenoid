package core.settings;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Reads all properties provided by Maven 
 */ 
public class MavenDefaults {
    /* Logging instance that is known by log4j configuration file */
    private static Logger log = LogManager.getLogger(MavenDefaults.class);
    /* {@link Properties} for loading the input stream containing the applications properties provided by Maven on runtime */
    private Properties properties;
    /* Singleton instance of the Class */
    private static MavenDefaults maven;
    /* Path to application.properties for reading Maven properties */
    public static final String APPLICATION_PATH = "/application.properties";

    /*
     * Hidden Constructor (needed for Singleton) to read the properties from system
     */
    private MavenDefaults() {
        try (InputStream is = this.getClass().getResourceAsStream(APPLICATION_PATH)) {
            properties = new Properties();
            properties.load(is);
        } catch (IOException e) {
            log.error("There is a failure on reading the properties out of " + APPLICATION_PATH
                    + ". It seems the file does not exist, the file is corrupted or the rights are set wrong.\n"
                    + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Initialize the {@link Properties} before defaults can be read. If already initialized will not read {@link Properties}.
     * 
     * @return {@link MavenDefaults}
     */
    public static MavenDefaults init() {
        return maven = (maven == null ? new MavenDefaults() : maven);
    }

    /**
     * Get a property by its key as a {@link String}
     * 
     * @param key
     * @return {@link String}
     */
    public String getProperty(String key) {
        return properties.getProperty(key, "");
    }

    /**
     * Get a property by its key as an {@link Integer}
     * 
     * @param key
     * @return {@link Integer}
     */
    public int getPropertyAsInteger(String key) {
        return Integer.parseInt(properties.getProperty(key, ""));
    }

    /**
     * Deliver the {@link Properties} object itself.
     * 
     * @return {@link Properties}
     */
    public Properties getAll() {
        return properties;
    }
    
    @Override
    public String toString() {
        Set<Object> keys = getAll().keySet();
        StringBuilder sb = new StringBuilder();
        for(Object key : keys)
            sb.append("{" + key + ":" + getProperty((String)key) + "}");
        String report = String.format("MavenDefaults [%s]", sb.toString());
        return report;
    }
}