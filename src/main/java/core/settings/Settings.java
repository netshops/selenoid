package core.settings;

import drivers.types.DesktopBrowser;
import drivers.types.DesktopOS;
import drivers.types.Mobile;
import drivers.types.Tablet;

/**
 * Default settings for the core environment
 * - Sauce Labs
 * - Environments
 * - User
 * 
 * @author Philip Taddey
 */
public class Settings {
    /** Remote Machine */
    public static final String REMOTE_URL = "";
    /** Sauce Labs Settings */
    public static final String APPIUM_VERSION = "1.5.2";
    public static final String SELENIUM_VERSION = "2.53.0";
    public static final String SCREEN_RESOLUTION = "1280x1024";
    public static final String SMALL_SCREEN_RESOLUTION = "1024x768";
    public static final int MAX_DURATION = 900;
    public static final int COMMAND_TIMEOUT = 150;
    public static final int IMPLICIT_WAIT = 10;
    public static final boolean POPUP_HANDLE = true;
    /** Default Environments */
    public static final String DEFAULT_BROWSER = DesktopBrowser.CHROME.name();
    public static final String DEFAULT_OS = DesktopOS.WINDOWS_7.name();
    public static final String DEFAULT_TABLET = Tablet.SAMSUNG_TAB_3.name();
    public static final String DEFAULT_DEVICE = Mobile.IPHONE_SIMULATOR.name();
    /** Sauce Labs Authentication */
    public static final String SAUCE_LABS_USER = "netshops";
    public static final String SAUCE_LABS_TOKEN = "5d509018-080d-496d-8874-45df827da538";
    public static final String SAUCE_LABS_URL = "http://%s:%s@ondemand.saucelabs.com:80/wd/hub";
    /** Remote Machines */
    public static final String REMOTE_ADDRESS = "http://6ce11246.ngrok.io/wd/hub";
}