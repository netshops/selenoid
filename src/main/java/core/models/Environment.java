package core.models;

import drivers.types.DesktopBrowser;
import drivers.types.DesktopOS;
import drivers.types.Mobile;
import drivers.types.ScreenResolution;
import drivers.types.Tablet;

/**
 * Configuration for test environment execution
 * 
 * @author Philip Taddey
 */
public class Environment {
    /* Flag for local testing */
    private boolean local;
    /* The browser with which the test should be executed */
    private DesktopBrowser browser;
    /* The browser version if needed (optional) */
    private String browserVersion;
    /* The OS with which the test should be executed */
    private DesktopOS os;
    /* The tablet with which the test should be executed */
    private Tablet tablet;
    /* The mobile with which the test should be executed */
    private Mobile mobile;
    /* The mobile platform version with which the test should be executed */
    private String platformVersion;
    /* The device orientation with which the test should be executed */
    private String orientation;
    /* The screen resolution with which the test should be executed */
    private ScreenResolution resolution;
    /* The implicit wait which should replace the default setting */
    private int implicitWait;
    /* The test suite marked for execution */
    private String suite;
    /* The job marked for execution */
    private String job;
    /* The build number of the execution project */
    private String build;
    /* The website to be tested */
    private String website;
    /* Flag for Jira Ticket Reporting */
    private boolean jira;
    /* TestRail Enabled / Disabled */
    private boolean testrail;
    /* TestRail Testrun Suite ID */
    private String testrailId;
    
    /**
     * Constructing the configuration for the test containing all possible settings (leave null, 0 if not needed)
     */
    public Environment(DesktopBrowser browser, DesktopOS os, Tablet tablet, Mobile mobile, String platformVersion, 
            String orientation, ScreenResolution resolution, int implicitWait, String suite, String job, String build, 
            String website, String browserVersion, boolean local, boolean jira, boolean testrail, String testrailId) {
        this.browser = browser;
        this.os = os;
        this.tablet = tablet;
        this.mobile = mobile;
        this.platformVersion = platformVersion;
        this.orientation = orientation;
        this.resolution = resolution;
        this.implicitWait = implicitWait;
        this.suite = suite;
        this.job = job;
        this.build = build;
        this.website = website;
        this.browserVersion = browserVersion;
        this.local = local;
        this.jira = jira;
        this.testrail = testrail;
        this.testrailId = testrailId;
    }

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    public DesktopBrowser getBrowser() {
        return browser;
    }

    public void setBrowser(DesktopBrowser browser) {
        this.browser = browser;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public DesktopOS getOs() {
        return os;
    }

    public void setOs(DesktopOS os) {
        this.os = os;
    }

    public Tablet getTablet() {
        return tablet;
    }

    public void setTablet(Tablet tablet) {
        this.tablet = tablet;
    }

    public Mobile getMobile() {
        return mobile;
    }

    public void setMobile(Mobile mobile) {
        this.mobile = mobile;
    }

    public ScreenResolution getResolution() {
        return resolution;
    }

    public void setResolution(ScreenResolution resolution) {
        this.resolution = resolution;
    }

    public int getImplicitWait() {
        return implicitWait;
    }

    public void setImplicitWait(int implicitWait) {
        this.implicitWait = implicitWait;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public boolean isJira() {
        return jira;
    }

    public void setJira(boolean jira) {
        this.jira = jira;
    }

    public boolean isTestrail() {
        return testrail;
    }

    public void setTestrail(boolean testrail) {
        this.testrail = testrail;
    }

    public String getTestrailId() {
        return testrailId;
    }

    public void setTestrailId(String testrailId) {
        this.testrailId = testrailId;
    }

    @Override
    public String toString() {
        return String.format("Configuration [browser=%s, os=%s, tablet=%s, mobile=%s, platformVersion=%s, orientation=%s, resolution=%s, implicitWait=%s, suite=%s, job=%s, "
                + "build=%s, website=%s, browserVersion=%s, local=%s, jira=%s, testrail=%s, testrailId=%s]", browser, os, tablet, mobile, platformVersion, orientation, resolution, 
                implicitWait, suite, job, build, website, browserVersion, local, jira, testrail, testrailId);
    }
}