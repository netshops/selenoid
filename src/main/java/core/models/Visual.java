package core.models;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import drivers.types.Mobile;
import drivers.types.Tablet;

/**
 * Configuration for visual regression testing
 * 
 * @author Philip Taddey
 */
public class Visual {
    /* Logging instance for Configuration */
    private static Logger log = LogManager.getLogger(Visual.class);
    /* The browser with which the test should be executed */
    private String browser;
    /* The browser version */
    private String browserVersion;
    /* The screen resolution */
    private String screenResolution;
    /* The OS with which the test should be executed */
    private String os;
    /* The Tablet with which the test should be executed */
    private Tablet tablet;
    /* The Mobile device with which the test should be executed */
    private Mobile mobile;
    /* The section marked for testing */
    private String section;
    /* The orientation of the device */
    private String orientation;

    /**
     * Constructing the configuration for the test containing all possible settings (leave null, 0 if not needed)
     */
    public Visual(String browser, String os, Tablet tablet, Mobile mobile, String section, String browserVersion, 
            String screenResolution, String orientation) {
        this.browser = browser;
        this.os = os;
        this.tablet = tablet;
        this.mobile = mobile;
        this.section = section;
        this.browserVersion = browserVersion;
        this.screenResolution = screenResolution;
        this.orientation = orientation;
        log.debug("Visual Regression Config: " + toString());
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public Tablet getTablet() {
        return tablet;
    }

    public void setTablet(Tablet tablet) {
        this.tablet = tablet;
    }

    public Mobile getMobile() {
        return mobile;
    }

    public void setMobile(Mobile mobile) {
        this.mobile = mobile;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }   

    public String getScreenResolution() {
        return screenResolution;
    }

    public void setScreenResolution(String screenResolution) {
        this.screenResolution = screenResolution;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    @Override
    public String toString() {
        return String.format("VisualConfig [browser=%s, os=%s, tablet=%s, mobile=%s, section=%s, "
                + "browserVersion=%s, screenResolution=%s, orientation=%s]", 
                browser, os, tablet, mobile, section, browserVersion, screenResolution, orientation);
    }
}