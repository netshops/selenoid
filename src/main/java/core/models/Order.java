package core.models;

/**
 * Product Model
 * Saves all required product data
 * 
 * @author Philip Taddey
 */
public class Order {
    private String articleId;
    private String articleName;
    private String articleBrand;
    private String articlePrice;
    private String articleSalePrice;
    private String articleVariant;
    private String articleSize;
    private String articleColor;
    private String articleDefaultImage;
    private String articleAdditionalSize;
    private String articleDelivery;
    private String articleQuantity;
    private String orderDate;
    private String orderNumber;
    private String orderTotal;
    private String orderPayment;
    private boolean isVoucher;
    private String voucher;
    
    public Order(String articleId, String articleName, String articleBrand, String articlePrice,
            String articleSalePrice, String articleVariant, String articleSize, String articleColor,
            String articleDefaultImage, String articleAdditionalSize, String articleDelivery, String articleQuantity,
            String orderDate, String orderNumber, String orderTotal, String orderPayment, boolean isVoucher,
            String voucher) {
        super();
        this.articleId = articleId;
        this.articleName = articleName;
        this.articleBrand = articleBrand;
        this.articlePrice = articlePrice;
        this.articleSalePrice = articleSalePrice;
        this.articleVariant = articleVariant;
        this.articleSize = articleSize;
        this.articleColor = articleColor;
        this.articleDefaultImage = articleDefaultImage;
        this.articleAdditionalSize = articleAdditionalSize;
        this.articleDelivery = articleDelivery;
        this.articleQuantity = articleQuantity;
        this.orderDate = orderDate;
        this.orderNumber = orderNumber;
        this.orderTotal = orderTotal;
        this.orderPayment = orderPayment;
        this.isVoucher = isVoucher;
        this.voucher = voucher;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getArticleBrand() {
        return articleBrand;
    }

    public void setArticleBrand(String articleBrand) {
        this.articleBrand = articleBrand;
    }

    public String getArticlePrice() {
        return articlePrice;
    }

    public void setArticlePrice(String articlePrice) {
        this.articlePrice = articlePrice;
    }

    public String getArticleSalePrice() {
        return articleSalePrice;
    }

    public void setArticleSalePrice(String articleSalePrice) {
        this.articleSalePrice = articleSalePrice;
    }

    public String getArticleVariant() {
        return articleVariant;
    }

    public void setArticleVariant(String articleVariant) {
        this.articleVariant = articleVariant;
    }

    public String getArticleSize() {
        return articleSize;
    }

    public void setArticleSize(String articleSize) {
        this.articleSize = articleSize;
    }

    public String getArticleColor() {
        return articleColor;
    }

    public void setArticleColor(String articleColor) {
        this.articleColor = articleColor;
    }

    public String getArticleDefaultImage() {
        return articleDefaultImage;
    }

    public void setArticleDefaultImage(String articleDefaultImage) {
        this.articleDefaultImage = articleDefaultImage;
    }

    public String getArticleAdditionalSize() {
        return articleAdditionalSize;
    }

    public void setArticleAdditionalSize(String articleAdditionalSize) {
        this.articleAdditionalSize = articleAdditionalSize;
    }

    public String getArticleDelivery() {
        return articleDelivery;
    }

    public void setArticleDelivery(String articleDelivery) {
        this.articleDelivery = articleDelivery;
    }

    public String getArticleQuantity() {
        return articleQuantity;
    }

    public void setArticleQuantity(String articleQuantity) {
        this.articleQuantity = articleQuantity;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getOrderPayment() {
        return orderPayment;
    }

    public void setOrderPayment(String orderPayment) {
        this.orderPayment = orderPayment;
    }

    public boolean isVoucher() {
        return isVoucher;
    }

    public void setVoucher(boolean isVoucher) {
        this.isVoucher = isVoucher;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    @Override
    public String toString() {
        return "Order [articleId=" + articleId + ", articleName=" + articleName + ", articleBrand=" + articleBrand
                + ", articlePrice=" + articlePrice + ", articleSalePrice=" + articleSalePrice + ", articleVariant="
                + articleVariant + ", articleSize=" + articleSize + ", articleColor=" + articleColor
                + ", articleDefaultImage=" + articleDefaultImage + ", articleAdditionalSize=" + articleAdditionalSize
                + ", articleDelivery=" + articleDelivery + ", articleQuantity=" + articleQuantity + ", orderDate="
                + orderDate + ", orderNumber=" + orderNumber + ", orderTotal=" + orderTotal + ", orderPayment="
                + orderPayment + ", isVoucher=" + isVoucher + ", voucher=" + voucher + "]";
    }
}