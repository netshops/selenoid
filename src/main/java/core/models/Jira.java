package core.models;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Jira {
    /* Logging instance */
    private static Logger log = LogManager.getLogger(Jira.class);
    /* Jira Project Name */
    private String jiraProject;
    /* General Project Name */
    private String projectName;
    /* Failure URL */
    private String failUrl;
    /* Server (Stage or Live) */
    private String server;
    /* Environment */
    private Environment env;
    /* Jira Summary */
    private String jiraSummary;
    /* Ticket Assignee*/
    private String assignee;
    /* Full Stack trace */
    private String stacktrace;
    /* Error Message */
    private String error;
    /* Test Start Date */
    private String startDate;
    /* Failure Screenshot */
    private File file;
    
    /**
     * Constructing the configuration for the test containing all possible settings (leave null, 0 if not needed)
     */
    public Jira(String jiraProject, String projectName, String failUrl, String server,
            Environment env, String jiraSummary, String assignee, String stacktrace, String error, String startDate, File file) {
        this.jiraProject = jiraProject;
        this.projectName = projectName;
        this.failUrl = failUrl;
        this.server = server;
        this.env = env;
        this.jiraSummary = jiraSummary;
        this.assignee = assignee;
        this.stacktrace = stacktrace;
        this.error = error;
        this.startDate = startDate;
        this.file = file;
        log.debug("Jira Config: " + toString());
    }

    public String getJiraProject() {
        return jiraProject;
    }

    public void setJiraProject(String jiraProject) {
        this.jiraProject = jiraProject;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getFailUrl() {
        return failUrl;
    }

    public void setFailUrl(String failUrl) {
        this.failUrl = failUrl;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public Environment getEnv() {
        return env;
    }

    public void setEnv(Environment env) {
        this.env = env;
    }

    public String getJiraSummary() {
        return jiraSummary;
    }

    public void setJiraSummary(String jiraSummary) {
        this.jiraSummary = jiraSummary;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
    }
    
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    
    @Override
    public String toString() {
        return "Jira [jiraProject=" + jiraProject + ", projectName=" + projectName + ", failUrl=" + failUrl
                + ", server=" + server + ", env=" + env + ", jiraSummary=" + jiraSummary + ", assignee=" + assignee
                + ", stacktrace=" + stacktrace + ", error=" + error + ", startDate=" + startDate + ", file=" + file
                + "]";
    }
}