package core.models;

/**
 * Model for saving specific user data 
 * If needed, feel free to add additional user data.
 * 
 * @author Philip Taddey
 */
public class Customer {
    private String customerId;
    private String email;
    private String password;
    private String salutation;
    private String firstname;
    private String lastname;
    private String birthday;
    private String birthmonth;
    private String birthyear;
    private String phone;
    private String street;
    private String streetnr;
    private String zip;
    private String city;
    private String address;
    
    public Customer(String customerId, String email, String password, String salutation, String firstname,
            String lastname, String birthday, String birthmonth, String birthyear, String phone, String street,
            String streetnr, String zip, String city, String address) {
        super();
        this.customerId = customerId;
        this.email = email;
        this.password = password;
        this.salutation = salutation;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.birthmonth = birthmonth;
        this.birthyear = birthyear;
        this.phone = phone;
        this.street = street;
        this.streetnr = streetnr;
        this.zip = zip;
        this.city = city;
        this.address = address;
    }
    
    public String getCustomerId() {
        return customerId;
    }
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getSalutation() {
        return salutation;
    }
    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public String getBirthday() {
        return birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public String getBirthmonth() {
        return birthmonth;
    }
    public void setBirthmonth(String birthmonth) {
        this.birthmonth = birthmonth;
    }
    public String getBirthyear() {
        return birthyear;
    }
    public void setBirthyear(String birthyear) {
        this.birthyear = birthyear;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getStreetnr() {
        return streetnr;
    }
    public void setStreetnr(String streetnr) {
        this.streetnr = streetnr;
    }
    public String getZip() {
        return zip;
    }
    public void setZip(String zip) {
        this.zip = zip;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    @Override
    public String toString() {
        return "Customer [customerId=" + customerId + ", email=" + email + ", password=" + password + ", salutation="
                + salutation + ", firstname=" + firstname + ", lastname=" + lastname + ", birthday=" + birthday
                + ", birthmonth=" + birthmonth + ", birthyear=" + birthyear + ", phone=" + phone + ", street=" + street
                + ", streetnr=" + streetnr + ", zip=" + zip + ", city=" + city + ", address=" + address + "]";
    }
}