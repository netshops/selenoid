package core.visualregression;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import core.helper.TestOutput;
import core.models.Visual;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

/**
 * Compare and take screenshots of a given page or webelement. 
 * Find differences for a existing and newly taken screenshot.
 * 
 * @author Philip Taddey
 */
public class VisualRunner {
    public static Logger log = LogManager.getLogger(VisualRunner.class);
    public static String SysDir = "target/visual-diff/";
    private static List<File> fileList = new ArrayList<>();
    
    /**
     * Load existing image
     */
    private static BufferedImage loadImage(String path) {
        try {
            return ImageIO.read(new File(path));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Setup filename for a given environment
     */
    private static String setupFile(Visual vconf, String name) {
        File dir = new File(SysDir + vconf.getSection());
        File sys = new File(SysDir);
        if (!sys.exists()) {
            log.trace("Creating directory: " + SysDir);
            sys.mkdir();
        }
        if (!dir.exists()) {
            log.trace("Creating directory: " + SysDir + vconf.getSection());
            dir.mkdir();
        }
        if (vconf.getTablet() != null) {
            String filename = SysDir + vconf.getSection() + "/" +  name + "_" + vconf.getTablet() + "_" + vconf.getOrientation();
            return filename;
        } else if (vconf.getMobile() != null) {
            String filename = SysDir + vconf.getSection() + "/" +  name + "_" +  vconf.getMobile() + "_" + vconf.getOrientation();
            return filename;
        } else {
            String filename = SysDir + vconf.getSection() + "/" +  name + "_" +  vconf.getBrowser() + "_" +  
            vconf.getBrowserVersion()  + "_" + vconf.getScreenResolution() + "_" +  vconf.getOs();
            return filename;
        }     
    }
    
    /**
     * Create a new screenshot of a page
     */
    private static File generateScreenshot(String filename, WebDriver driver) throws IOException {
        log.trace("I generate a new screenshot as " + filename + ".png");
        Screenshot screenshot = new AShot()
            .takeScreenshot(driver);
        File outputfile = new File(filename + ".png");
        ImageIO.write(screenshot.getImage(), "png", outputfile);
        return outputfile;
    }
    
    /**
     * Save actual screenshot as existing screenshot
     */
    private static File saveExpectedScreenshot(Screenshot screenshot, String filename, WebDriver driver) throws IOException {
        log.trace("I save the expected screenshot as " + filename + "_EXPECTED.png");
        File outputfile = new File(filename + "_EXPECTED.png");
        ImageIO.write(screenshot.getImage(), "png", outputfile);
        fileList.add(outputfile);
        return outputfile;
    }
    
    /**
     * Save actual screenshot as existing screenshot
     */
    private static File saveActualScreenshot(Screenshot screenshot, String filename, WebDriver driver) throws IOException {
        log.trace("I overrite the existing screenshot with the actual state as " + filename + ".png");
        File outputfile = new File(filename + ".png");
        ImageIO.write(screenshot.getImage(), "png", outputfile);
        fileList.add(outputfile);
        return outputfile;
    }
    
    /**
     * Save error screenshot
     */
    private static File saveErrorScreenshot(ImageDiff diff, String filename, WebDriver driver) throws IOException {
        log.trace("Found diff and saved failure screenshot as " + filename + "_DIFFERENCE.png");
        BufferedImage diffImage = diff.getMarkedImage();
        File outputfile = new File(filename + "_DIFFERENCE.png");
        ImageIO.write(diffImage, "png", outputfile);
        fileList.add(outputfile);
        return outputfile;
    }
    
    /**
     * Create a new screenshot of a given weblement
     */
    private static File generateElementScreenshot(String filename, WebDriver driver, WebElement element) throws IOException {
        log.trace("I generate a new screenshot as " + filename + ".png");
        Screenshot screenshot = new AShot()
            .takeScreenshot(driver, element);
        File outputfile = new File(filename + ".png");
        ImageIO.write(screenshot.getImage(), "png", outputfile);
        return outputfile;
    }
    
    /**
     * Create a new screenshot of a given web element with a set of ignored web elements
     */
    public static File generateElementScreenshotIgnored(String filename, WebDriver driver, WebElement element, Set<By> ignoredElements) throws IOException {
        log.trace("I generate a new screenshot as " + filename + ".png");
        Screenshot screenshot = new AShot()
            .ignoredElements(ignoredElements)
            .takeScreenshot(driver, element);
        File outputfile = new File(filename + ".png");
        ImageIO.write(screenshot.getImage(), "png", outputfile);
        return outputfile;
    }
    
    /**
     * Create a new screenshot of multiple weblements
     */
    public static File generateMultipleElementScreenshot(String filename, WebDriver driver, List<WebElement> elements) throws IOException {
        log.trace("I generate a new screenshot as " + filename + ".png");
        Screenshot screenshot = new AShot()
            .takeScreenshot(driver, elements);
        File outputfile = new File(filename + ".png");
        ImageIO.write(screenshot.getImage(), "png", outputfile);
        return outputfile;
    }
    
    /**
     * Create a new screenshot of multiple weblements
     */
    public static File generateMultipleElementScreenshotIgnored(String filename, WebDriver driver, List<WebElement> elements, Set<By> ignoredElements) throws IOException {
        log.trace("I generate a new screenshot as " + filename + ".png");
        Screenshot screenshot = new AShot()
            .ignoredElements(ignoredElements)
            .takeScreenshot(driver, elements);
        File outputfile = new File(filename + ".png");
        ImageIO.write(screenshot.getImage(), "png", outputfile);
        return outputfile;
    }   
    /**
     * Find differences for a whole page
     * 
     * @param vconf Environment Setup
     * @param driver WebDriver
     * @throws IOException
     */
    public void comparePage(Visual vconf, String screenshotName, WebDriver driver) throws IOException {
        String filename = setupFile(vconf, screenshotName);
        File file = new File(filename + ".png");
        if (file.exists()) {
            TestOutput.log("I assert that there are no visual changes for " + screenshotName);
            BufferedImage EXISTING_IMAGE = loadImage(filename + ".png");
            Screenshot existing_screenshot = new Screenshot(EXISTING_IMAGE);        
            Screenshot actual_screenshot = new AShot()
                .takeScreenshot(driver);
            ImageDiff diff = new ImageDiffer().makeDiff(existing_screenshot, actual_screenshot);
            if (diff.hasDiff()) {
                saveErrorScreenshot(diff, filename, driver);
                saveExpectedScreenshot(existing_screenshot, filename, driver);
            }
            saveActualScreenshot(actual_screenshot, filename, driver);
            Assert.assertFalse(diff.hasDiff());
        } else {
            generateScreenshot(filename, driver);
         }
    }
    
    /**
     * Find difference for a specific webelement
     */
    public List<File> compareElement(Visual vconf, String screenshotName, WebDriver driver, WebElement element) throws IOException {
        String filename = setupFile(vconf, screenshotName);
        List<File> screenshots = new ArrayList<File>();
        File file = new File(filename + ".png");
        if (file.exists()) {
            TestOutput.log("I assert that there are no visual changes for " + screenshotName);
            BufferedImage EXISTING_IMAGE = loadImage(filename + ".png");
            Screenshot existing_screenshot = new Screenshot(EXISTING_IMAGE);        
            Screenshot actual_screenshot = new AShot()
                .takeScreenshot(driver, element);
            ImageDiff diff = new ImageDiffer()
                    .makeDiff(existing_screenshot, actual_screenshot)
                    .withDiffSizeTrigger(2);
            screenshots.add(saveActualScreenshot(actual_screenshot, filename, driver));
            if (diff.hasDiff()) {
                screenshots.add(saveErrorScreenshot(diff, filename, driver));
                screenshots.add(saveExpectedScreenshot(existing_screenshot, filename, driver));
            }
            Assert.assertFalse(diff.hasDiff());
        } else {
            generateElementScreenshot(filename, driver, element);
         }
        return fileList;
    }
    
    /**
     * Find difference for a specific web element without ignored elements 
     * 
     * @param vconf Environment Setup
     * @param driver WebDriver
     * @param element WebElement
     * @throws IOException
     */
    public void compareElementWithIgnored(Visual vconf, String screenshotName, WebDriver driver, WebElement element, Set<By> ignoredElements) throws IOException {
        String filename = setupFile(vconf, screenshotName);
        File file = new File(filename + ".png");
        if (file.exists()) {
            TestOutput.log("I assert that there are no visual changes for " + screenshotName);
            BufferedImage EXISTING_IMAGE = loadImage(filename + ".png");
            Screenshot existing_screenshot = new Screenshot(EXISTING_IMAGE);        
            Screenshot actual_screenshot = new AShot()
                .ignoredElements(ignoredElements)
                .takeScreenshot(driver, element);
            ImageDiff diff = new ImageDiffer()
                    .makeDiff(existing_screenshot, actual_screenshot)
                    .withDiffSizeTrigger(2);
            if (diff.hasDiff()) {
                saveErrorScreenshot(diff, filename, driver);
                saveExpectedScreenshot(existing_screenshot, filename, driver);
            }
            saveActualScreenshot(actual_screenshot, filename, driver);
            Assert.assertFalse(diff.hasDiff());
        } else {
            generateElementScreenshotIgnored(filename, driver, element, ignoredElements);
         }
    }
    
    /**
     * Find difference for a specific webelement
     * 
     * @param vconf Environment Setup
     * @param driver WebDriver
     * @param element WebElement
     * @throws IOException
     */
    public void compareMultipleElements(Visual vconf, String screenshotName, WebDriver driver, List<WebElement> element) throws IOException {
        String filename = setupFile(vconf, screenshotName);
        File file = new File(filename + ".png");
        if (file.exists()) {
            TestOutput.log("I assert that there are no visual changes for " + screenshotName);
            BufferedImage EXISTING_IMAGE = loadImage(filename + ".png");
            Screenshot existing_screenshot = new Screenshot(EXISTING_IMAGE);        
            Screenshot actual_screenshot = new AShot()
                .takeScreenshot(driver, element);
            ImageDiff diff = new ImageDiffer()
                    .makeDiff(existing_screenshot, actual_screenshot)
                    .withDiffSizeTrigger(2);
            if (diff.hasDiff()) {
                saveErrorScreenshot(diff, filename, driver);
                saveExpectedScreenshot(existing_screenshot, filename, driver);
            }
            saveActualScreenshot(actual_screenshot, filename, driver);
            Assert.assertFalse(diff.hasDiff());
        } else {
            generateMultipleElementScreenshot(filename, driver, element);
         }
    }
    
    /**
     * Find difference for a specific webelement
     */
    public void compareMultipleElementsWithIgnored(Visual vconf, String screenshotName, WebDriver driver, List<WebElement> element, Set<By> ignoredElements) throws IOException {
        String filename = setupFile(vconf, screenshotName);
        File file = new File(filename + ".png");
        if (file.exists()) {
            TestOutput.log("I assert that there are no visual changes for " + screenshotName);
            BufferedImage EXISTING_IMAGE = loadImage(filename + ".png");
            Screenshot existing_screenshot = new Screenshot(EXISTING_IMAGE);        
            Screenshot actual_screenshot = new AShot()
                .ignoredElements(ignoredElements)
                .takeScreenshot(driver, element);
            ImageDiff diff = new ImageDiffer()
                    .makeDiff(existing_screenshot, actual_screenshot)
                    .withDiffSizeTrigger(2);
            if (diff.hasDiff()) {
                saveErrorScreenshot(diff, filename, driver);
                saveExpectedScreenshot(existing_screenshot, filename, driver);
            }
           saveActualScreenshot(actual_screenshot, filename, driver);
           Assert.assertFalse(diff.hasDiff());
        } else {
            generateMultipleElementScreenshotIgnored(filename, driver, element, ignoredElements);
         }
    }
}