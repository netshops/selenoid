package core.plugins;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import core.helper.TestOutput;

/**
 * Link Checker - Version 2 
 * 
 * @author Philip Taddey
 */
public class LinkChecker {
    public Logger log = LogManager.getLogger(LinkChecker.class);
    private HashSet<String> alreadyListed;
    private Queue<String> queue;
    private Queue<String> tmpQueue;
    private Queue<String> errorQueue;
    private WebDriver driver;

    public LinkChecker(WebDriver driver) {
        this.driver = driver;  
        alreadyListed = new HashSet<String>();
        queue = new LinkedList<String>();
        tmpQueue = new LinkedList<String>();
        errorQueue = new LinkedList<String>();
    }
    
    private boolean isDomainValid(String href, String domain) {
        return href.startsWith(domain) && !href.contains("#")
                && !href.endsWith(".jpg") && !href.endsWith(".jpeg")
                && !href.endsWith(".pdf") && !href.endsWith(".png") && !href.endsWith(".gif") 
                && !href.endsWith(".css") && !href.endsWith(".js") && !href.endsWith(".flv");
    }
    
    private void isLinkAccessible(String linkUrl, String sourceUrl){
        try {
           URL url = new URL(linkUrl);
           HttpURLConnection connect = (HttpURLConnection)url.openConnection();
           connect.setConnectTimeout(3000);   
           connect.connect();
           if (connect.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND 
                   || connect.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED
                   || connect.getResponseCode() == HttpURLConnection.HTTP_BAD_GATEWAY
                   || connect.getResponseCode() == HttpURLConnection.HTTP_UNAVAILABLE
                   || connect.getResponseCode() == HttpURLConnection.HTTP_GATEWAY_TIMEOUT 
                   || connect.getResponseCode() == HttpURLConnection.HTTP_INTERNAL_ERROR ) {
               TestOutput.log("Page not found (" + connect.getResponseCode()  + "): " + linkUrl + " - " + connect.getResponseMessage()
                       + " - Source: " + sourceUrl);
               errorQueue.add(linkUrl);
            } //else {
              //  TestOutput.log("Valid: " + linkUrl);
            //}
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    } 
    
    /**
     * Link Checker for specific page
     */
    public boolean checkPage(String address) {
        TestOutput.log("Start crawling...");
        queue.add(address);
        //TestOutput.log("Open URL: " + address);
        driver.get(address);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        List<WebElement> linksList = driver.findElements(By.tagName("a"));
        for (int index=0; index<linksList.size(); index++ ) { 
            String href = driver.findElements(By.tagName("a")).get(index).getAttribute("href");
            if ((href != null) && (isDomainValid(href, address))) {
                if (!alreadyListed.contains(href)) {
                    alreadyListed.add(href);
                    queue.add(href);
                    tmpQueue.add(href);
                    if (!tmpQueue.isEmpty()) {
                        Iterator<String> listIterator = tmpQueue.iterator();
                        while (listIterator.hasNext()) {
                            isLinkAccessible(listIterator.next().toString(), address.toString());
                        }
                        tmpQueue.removeAll(tmpQueue);
                    }
                }
            }
        }
        TestOutput.log(queue.size() + " links found.");
        TestOutput.log(errorQueue.size() + " pages with errors found.");
        return !errorQueue.isEmpty() ? false : true;
    }
    
    /**
     * Link Checker for all pages
     */
    public boolean checkAllPages(String address) {
        TestOutput.log("Start crawling...");
        queue.add(address);
        String newAddress;
        while ((newAddress = queue.poll()) != null) {
            //TestOutput.log("Open URL: " + newAddress);
            driver.get(newAddress);
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            List<WebElement> linksList = driver.findElements(By.tagName("a"));
            for (int index=0; index<linksList.size(); index++ ) { 
                String href = driver.findElements(By.tagName("a")).get(index).getAttribute("href");
                if ((href != null) && (isDomainValid(href, address))) {
                    if (!alreadyListed.contains(href)) {
                        alreadyListed.add(href);
                        queue.add(href);
                        tmpQueue.add(href);
                        if (!tmpQueue.isEmpty()) {
                            Iterator<String> listIterator = tmpQueue.iterator();
                            while (listIterator.hasNext()) {
                                isLinkAccessible(listIterator.next().toString(), newAddress.toString());
                            }
                            tmpQueue.removeAll(tmpQueue);
                        }
                    }
                }
            }
        }
        TestOutput.log(queue.size() + " links found.");
        TestOutput.log(errorQueue.size() + " pages with errors found.");
        return !errorQueue.isEmpty() ? false : true;
    }
}