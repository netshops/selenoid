package core.plugins;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.models.Jira;
import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.Field;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;

/**
 * Jira Ticket Reporting
 * 
 * @author Philip Taddey
 */
public class JiraReport {
    public static Logger log = LogManager.getLogger(JiraReport.class);
    private static BasicCredentials creds;
    private static JiraClient jira;
    private String user;
    private String password;
    private String url;
    
    public JiraReport(String user, String password, String url) {
        this.setUser(user);
        this.setPassword(password);
        this.setUrl(url);
        creds = new BasicCredentials(user, password);
        jira = new JiraClient(url, creds);
    }
    
    /**
     * Create a new ticket summary
     * 
     * @param ticket
     * @return Jira Summary
     */
    public static String createSummary(Jira ticket) {
        ticket.setJiraSummary(String.format("[TEST AUTOMATION][%s][%s] %s", ticket.getProjectName(), ticket.getServer(), ticket.getJiraSummary()));
        return ticket.getJiraSummary();
    }
    /**
     * Description Content for Jira Ticket
     * 
     * @param ticket
     * @return String Content
     */
    private static String addDescription(Jira ticket) {
        String description = "*URL:* " + ticket.getFailUrl();
        if (ticket.getEnv().getTablet() != null) {
            description = description + "\n*Tablet:* " + ticket.getEnv().getTablet();
        } else if (ticket.getEnv().getMobile() != null) {
            description = description + "\n*Mobile:* " + ticket.getEnv().getMobile();
        } else if (ticket.getEnv().isLocal()) {
            description = description + "\n*Browser:* " + ticket.getEnv().getBrowser();
        } else {
            description = description + String.format("\n*Browser:* %s\n*Browser-Version:* %s\n*Betriebssystem:* %s\n*Screen-Resolution:* %s",
                    ticket.getEnv().getBrowser(), ticket.getEnv().getBrowserVersion(), ticket.getEnv().getOs(), ticket.getEnv().getResolution());
        }
        description = description + "\n*Test Started:* " + ticket.getStartDate();
        description = description + "\n*Error:* " + ticket.getError();
        description = description + "\n\n*Test Steps:*\n {code}" + ticket.getStacktrace() + "{code}";
        description = description + "\n\n\n_Hinweis: Dieses Ticket wurde über einen automatisierten Test erstellt._";
        return description;
    }

    /**
     * Report ticket in Jira
     * 
     * @param ticket Content for ticket
     */
    public static void reportIssue(Jira ticket) {
        try {
            Issue newIssue = jira.createIssue(ticket.getJiraProject(), "Bug")
                .field(Field.SUMMARY, ticket.getJiraSummary())
                .field(Field.DESCRIPTION, addDescription(ticket))
                .field(Field.ASSIGNEE, ticket.getAssignee())
                .execute();
            newIssue.addAttachment(ticket.getFile());
            log.trace("Jira Issue created: " + newIssue);
        } catch (JiraException ex) {
            log.trace(ex.getMessage());
            if (ex.getCause() != null)
                log.trace("Failed to create Jira Issue: " + ex.getCause().getMessage());
        }
    }
    
    /**
     * Find Jira issue and add comment if issue already exists
     * 
     * @param ticket
     * @return
     */
    public static boolean findIssue(Jira ticket) {
        Issue.SearchResult sr;
        try {
            sr = jira.searchIssues("summary=" + ticket.getJiraSummary());
            log.trace("Total Issues: " + sr.total);
            return sr.total == 0 ? false : true;
        } catch (JiraException ex) {
            log.trace(ex.getMessage());
            if (ex.getCause() != null)
                log.trace("Failed to find Jira Issue: " + ex.getCause().getMessage());
        }
        return false;
    }
    
    /**
     * Find Jira issue and add comment if issue already exists
     * 
     * @param ticket
     * @return
     */
    public static void addComment(Jira ticket) {
        try {
            Issue issue = jira.getIssue(ticket.getJiraSummary());
            issue.addComment("Test erneut fehlgeschlagen!");
        } catch (JiraException ex) {
            log.trace(ex.getMessage());
            if (ex.getCause() != null)
                log.trace("Failed to find Jira Issue: " + ex.getCause().getMessage());
        }
    }
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
}