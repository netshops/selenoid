package core.exceptions;

/**
 * TestRail Exception Handler
 * 
 * @author Philip Taddey
 */
public class TestRailAPIException extends Exception
{
    private static final long serialVersionUID = -8885597567918689321L;

    public TestRailAPIException(String message)
	{
		super(message);
	}
}