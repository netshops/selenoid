package core.exceptions;

/**
 * Exception Handler for Drivers
 */
public class DriverFactoryException extends Exception {
    private static final long serialVersionUID = 5742848235268466046L;

    /**
     * Simple Exception Handling by Exception without modification and enhanced messaging.
     */
    public DriverFactoryException() {
        super();
    }

    /**
     * Exception Handling by Exception with modified message. 
     * 
     * @param message
     */
    public DriverFactoryException(String message) {
        super(message);
    }
}